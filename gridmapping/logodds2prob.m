function [ p p_ ] = logodds2prob( log_odds )
%LOGODDS2PROB Summary of this function goes here
%   Detailed explanation goes here
p = ones(size(log_odds))./(ones(size(log_odds))+exp(-log_odds));

p_ = 1-p;

end

