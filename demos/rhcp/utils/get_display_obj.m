function display_obj = get_display_obj( problem, world_map, planner_params, record_video )
%GET_DISPLAY_OBJ Summary of this function goes here
%   Detailed explanation goes here

display_obj = struct();

display_obj.handle_set = struct();

% Figure 1: Main display
display_obj.handle_set.fig_main = figure;
hold on;
colormap(gray);
clims = [0 1];
display_obj.handle_set.occ_map = imagesc(zeros(size(world_map.data')), clims);
display_obj.handle_set.path_set = plot_path_set( transform_path_set(  problem.start, planner_params.path_set ) );
display_obj.handle_set.chosen = plot_path( problem.start );
display_obj.handle_set.history = plot_history( problem.start );
display_obj.handle_set.start_goal = plot_start_goal(problem.start, problem.goal, problem.goal_tol, world_map);
axis xy;
axis equal;

% Figure 2: Heuristic display
display_obj.handle_set.fig_heur = figure;
display_obj.handle_set.heur_map = imagesc(zeros(size(world_map.data')));
axis xy;
axis equal;

% Figure 3: WorldMap display
display_obj.handle_set.fig_world = figure;
display_obj.handle_set.world_map = imshow(world_map.data');
display_obj.handle_set.world_path_set = plot_path_set( transform_path_set( problem.start, planner_params.path_set ) );
display_obj.handle_set.world_chosen = plot_path( problem.start );
display_obj.handle_set.world_history = plot_history( problem.start );
display_obj.handle_set.world_start_goal = plot_start_goal(problem.start, problem.goal, problem.goal_tol, world_map);
axis xy;
axis equal;



display_obj.record_video = record_video;
if (record_video)
    display_obj.vidObj1 = VideoWriter('vid_robot.avi');
    open(display_obj.vidObj1);
    display_obj.vidObj2 = VideoWriter('vid_heur.avi');
    open(display_obj.vidObj2);
end


end

