function [ path, valid_plan, nodes,end_point ] = greedy_graph_info_planner(planner_params,grid_map,grid_params,distance_map,map_struct,laser_params,nodes,current_state,velocity,cur_goal_pt)
%EVALUATE_NODES Summary of this function goes here
%   Detailed explanation goes here   
valid_plan = 0;
connected = 0;

nodes.value = estimate_info_gain(grid_map.data,grid_params,map_struct,nodes.pos,laser_params);
start_coord = [current_state.x current_state.y];

threshold_map = distance_map.data;
threshold_map(threshold_map<=2)=0;
threshold_map(threshold_map>2)=1;

fmm_map = grid_map;
fmm_map.data = [];
start_id = coord2index(start_coord(1),start_coord(2),fmm_map.scale,fmm_map.min(1),fmm_map.min(2));
fmm_map.data = get_value_function( threshold_map, start_id(1), start_id(2));
end_id = coord2index(nodes.pos(:,1),nodes.pos(:,2),fmm_map.scale,fmm_map.min(1),fmm_map.min(2));

distance = fmm_map.data(sub2ind(size(fmm_map.data),end_id(:,1),end_id(:,2)));
nodes.value = 10*nodes.value - distance;

[nodes.value, idx]= sort(nodes.value,'descend');
nodes.pos = nodes.pos(idx,:);
count=1;


while(connected==0 && count<=size(nodes.pos,1))
    dist = sqrt((current_state.x-cur_goal_pt(1))*(current_state.x-cur_goal_pt(1)) + (current_state.y-cur_goal_pt(2))*(current_state.y-cur_goal_pt(2)));
    disp(dist);
    if (dist < 20)
        end_point = nodes.pos(count,1:2);
% path = get_path_from_line( [current_state.x current_state.y], end_point, velocity, planner_params);
% connected = check_connectivity(path,grid_map);
    else
        end_point = cur_goal_pt;
    end
    [path, connected ] = get_path_fmm( fmm_map, start_coord, end_point);
    if (connected == 0)
        [path, connected ] = get_path_fmm( fmm_map, start_coord, nodes.pos(count,1:2));
    end
    count = count + 1;
end
 

if(connected==0 && count>size(nodes.pos,1))
    error('Ran out of nodes and found no connection');
    valid_plan = 0;
else
    valid_plan =1;
end

