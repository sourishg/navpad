function [ connected ] = check_connectivity( path,map_struct)
%CHECK_CONNECTIVITY Summary of this function goes here
%   Detailed explanation goes here
[ID] = coord2index(path.x',path.y',map_struct.scale,map_struct.min(1),map_struct.min(2));
ID(ID(:,1)>size(map_struct.data,1),:)=[];
ID(ID(:,2)>size(map_struct.data,2),:)=[];
ID(ID(:,1)<1,:)=[];
ID(ID(:,2)<1,:)=[];

IDx = sub2ind(size(map_struct.data),ID(:,1),ID(:,2));
id=find (map_struct.data(IDx)>0);
if (size(id,1)>0)
    connected = 0;
else
    connected=1;
end
end

