%% Create an example giant path set for dubin type vehicle
clc
clear
close all

%% Define global paramters
global v_max;
global g;

v_max = 12;
g = 9.81;


%% Define dynamics
f_dyn = @(x, u) [v_max.*cos(x(3)); v_max.*sin(x(3)); (g/v_max)*tan(u(1))];

%% Define metadata
metadata = struct('x', 0, 'y', 0, 'psi', 0);

%% Define control input specification
phi_max = deg2rad(15);
phi_min = -phi_max;
phi_res = (phi_max - phi_min)/6;

%% Define time series
dt = 0.05;
time_sample = 0:1:4;

%% Define origin
x0 = [0 0 0];

%% Create path set
[ path_set ] = create_giant_pathset( f_dyn, x0, phi_max, phi_min, phi_res, time_sample, dt, metadata);