function state = state_at_index( traj, ind )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

state = struct(traj);
for field = fieldnames(state)'
    state.(field{1}) = traj.(field{1})(ind);
end

end

