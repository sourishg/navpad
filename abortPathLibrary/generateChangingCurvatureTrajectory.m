function [X,Y,time] = generateChangingCurvatureTrajectory(vMax,rollMaxAngle,decceleration,rollRate)
%PLOTCHANGINGCURVATURETRAJECTORY Summary of this function goes here
%   Detailed explanation goes here
numPoints = 1000;
g = 10;
v = vMax;
a = decceleration;
roll = 0;
X = zeros(numPoints+1,1);
Y = zeros(numPoints+1,1);
x = 0;
y = 0;
w = 0;
theta = 0;
T = vMax/a;
resolution = T/numPoints;
j = 1;
time = [0:resolution:T]';
for i=0:resolution:T
    X(j) = x;
    Y(j) = y;
    j = j+1;
    
    roll = rollRate*i;
    v = vMax - a*i;
    if roll > rollMaxAngle
        roll = rollMaxAngle;
    end
    if v < 0
        v  = 0;
    end
    if roll ~= 0 && v ~=0
        R = (v.*v)/(g*tan(roll));
        w = v/R;
    else
        R = 0;
        w  = 0;
    end
    theta = theta + w*resolution;
    x = x+ (v*resolution)*cos(theta);
    y = y+ (v*resolution)*sin(theta);
    
end


