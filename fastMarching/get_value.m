function [ val ] = get_value( V, state_x, state_y )
%GET_VALUE Summary of this function goes here
%   Detailed explanation goes here
l = [max(1,min(floor(state_x), size(V,1))); max(1,min(floor(state_y), size(V,2)))];
val = V(l(1),l(2));

end

